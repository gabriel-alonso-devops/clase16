# Usa una imagen base de nginx
FROM nginx:alpine

# Copia los archivos de configuración de nginx y otros archivos necesarios
COPY nginx/index.html /usr/share/nginx/html/index.html
COPY nginx/styles.css /usr/share/nginx/html/styles.css

# Exponer el puerto 80 para nginx
EXPOSE 80
